﻿using MailManager.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using wpfTask1;

namespace MailManager
{
    public partial class LoginWindow : Window
    {
        private LoginViewModel loginViewModel;
        public EmailUser User { get; set; }

        public LoginWindow()
        {
            loginViewModel = new LoginViewModel();
            this.DataContext = loginViewModel;
            InitializeComponent();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Ok(object sender, RoutedEventArgs e)
        {
            if (EmailData.GetUserData(loginViewModel.Login, passwordBox.Password, out EmailUser loggedUser))
            {
                User = loggedUser;
                DialogResult = true;
                Close();
            }
            else
            {
                MessageBox.Show(Application.Current.Resources["LoginFailed"].ToString());
            }
        }

        private void LoginChanged(object sender, TextChangedEventArgs e)
        {
            loginViewModel.LoginEnabled = loginViewModel.Login.Length > 0 && passwordBox.Password.Length > 0;
        }

        private void PasswordChanged(object sender, RoutedEventArgs e)
        {
            loginViewModel.LoginEnabled = loginViewModel.Login.Length > 0 && passwordBox.Password.Length > 0;
        }
    }
}
