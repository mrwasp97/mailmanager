﻿using MailManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using wpfTask1;

namespace MailManager
{
    /// <summary>
    /// Interaction logic for MailWindow.xaml
    /// </summary>
    public partial class MailWindow : Window
    {
        private MailViewModel viewModel;

        public EmailMessage NewMail { get; set; }

        public MailWindow(Window owner)
        {
            viewModel = new MailViewModel();
            this.DataContext = viewModel;
            this.Owner = owner;
            this.Height = 0.8 * Owner.ActualHeight;
            this.Width = 0.8 * Owner.ActualWidth;
            InitializeComponent();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Send(object sender, RoutedEventArgs e)
        {
            viewModel.NewEmail.Date = DateTime.Today.ToShortDateString();
            NewMail = viewModel.NewEmail;
            DialogResult = true;
            Close();
        }
    }
}
