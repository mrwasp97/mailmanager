﻿using MailManager.Properties;
using MailManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfTask1;

namespace MailManager
{
    public partial class MainWindow : Window
    {
        private MainViewModel viewModel;

        public MainWindow()
        {
            viewModel = new MainViewModel();
            this.DataContext = viewModel;
            InitializeComponent();
        }

        private void ShowLoginDialog(object sender, RoutedEventArgs e)
        {
            if (viewModel.LoggedUser == null)
            {
                this.Opacity = 0.5;
                LoginWindow dialog = new LoginWindow();
                dialog.Owner = Window.GetWindow(this);
                if (dialog.ShowDialog() ?? false)
                {
                    viewModel.Login(dialog.User);
                    this.Resources["ReceivedMessages"] = viewModel.FilterReceivedMessages();
                    this.Resources["SentMessages"] = viewModel.FilterSentMessages();
                }
                this.Opacity = 1.0;
            }
            else
            {
                viewModel.Logout();
            }
        }
        private void ChooseMessage(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            viewModel.SelectedMessage = listBox.SelectedItem as EmailMessage;
        }

        private void FilterMessages(object sender, TextChangedEventArgs e)
        {
            this.Resources["ReceivedMessages"] = viewModel.FilterReceivedMessages();
            this.Resources["SentMessages"] = viewModel.FilterSentMessages();
        }

        private void ShowMailDialog(object sender, RoutedEventArgs e)
        {
            this.Opacity = 0.5;
            MailWindow mailDialog = new MailWindow(GetWindow(this));
            //mailDialog.Owner = GetWindow(this);
            if (mailDialog.ShowDialog() ?? false)
            {
                viewModel.SendMessage(mailDialog.NewMail);
                this.Resources["SentMessages"] = viewModel.FilterSentMessages();
            }
            this.Opacity = 1.0;
        }

        private void EnglishLanguage(object sender, RoutedEventArgs e)
        {
            ChangeLanguage("en-US");
        }

        private void PolishLanguage(object sender, RoutedEventArgs e)
        {
            ChangeLanguage("pl-PL");
        }

        private void ChangeLanguage(string lang)
        {
            CultureInfo cultureInfo = new CultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture = cultureInfo;



            var rd = new ResourceDictionary() { Source = new Uri($"StringResources.{lang}.xaml", UriKind.RelativeOrAbsolute) };

            //if (this.Resources.MergedDictionaries.Count == 0)
            //    this.Resources.MergedDictionaries.Add(rd);
            //else
            //    this.Resources.MergedDictionaries[0] = rd;

            //if (App.Current.Resources.MergedDictionaries.Count == 0)
            //    App.Current.Resources.MergedDictionaries.Add(rd);
            //else
            //    App.Current.Resources.MergedDictionaries[0] = rd;

            Application.Current.Resources.MergedDictionaries.Add(rd);

        }

        private void DeleteReceived(object sender, KeyEventArgs e)
        {
            ListBox receivedListBox = (ListBox)sender;
            if (e.Key == Key.Delete)
            {
                if (receivedListBox.SelectedIndex != -1)
                {
                    viewModel.DeleteReceived((EmailMessage)receivedListBox.SelectedItem);
                    this.Resources["ReceivedMessages"] = viewModel.FilterReceivedMessages();
                }
            }
        }

        private void DeleteSent(object sender, KeyEventArgs e)
        {
            ListBox sentListBox = (ListBox)sender;
            if (e.Key == Key.Delete)
            {
                if (sentListBox.SelectedIndex != -1)
                {
                    viewModel.DeleteSent((EmailMessage)sentListBox.SelectedItem);
                    this.Resources["SentMessages"] = viewModel.FilterSentMessages();
                }
            }
        }
    }
}
