﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailManager.ViewModels
{
    public class LoginViewModel : ViewModel
    {
        private string login;
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
                OnPropertyChanged("Login");
            }
        }

        private bool loginEnabled;
        public bool LoginEnabled
        {
            get
            {
                return loginEnabled;
            }
            set
            {
                loginEnabled = value;
                OnPropertyChanged("LoginEnabled");
            }
        }

    }
}
