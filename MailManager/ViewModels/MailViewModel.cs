﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using wpfTask1;

namespace MailManager.ViewModels
{
    public class MailViewModel : ViewModel
    {
        private EmailMessage newEmail;

        public EmailMessage NewEmail
        {
            get
            {
                return newEmail;
            }
            set
            {
                newEmail = value;
                OnPropertyChanged("NewEmail");
            }
        }

        public MailViewModel()
        {
            NewEmail = new EmailMessage();
        }
    }

    public class ResizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0.8 * (double)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsValidConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string recipient = (string)values[0] ?? "";
            bool recipientIsValid = recipient.Trim().Length > 0
                && new Regex(RecipientValidator.EMAIL_REGEX).Match(recipient).Success;

            string title = (string)values[1] ?? "";
            bool titleIsValid = title.Trim().Length > 0;

            string body = (string)values[2] ?? "";
            bool bodyIsValid = body.Trim().Length > 0
                && body.Length >= 10;

            return recipientIsValid
                && titleIsValid
                && bodyIsValid;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RecipientValidator : ValidationRule
    {
        public const string EMAIL_REGEX = "\\w+@\\w+\\.\\w{2,}";
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string recipient = (string)value ?? "";
            if (recipient.Trim().Length == 0)
            {
                return new ValidationResult(false, App.Current.Resources["MailValidationEmpty"].ToString());
            }
            else if (!new Regex(EMAIL_REGEX).Match(recipient).Success)
            {
                return new ValidationResult(false, App.Current.Resources["MailValidationAddress"].ToString());
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class TitleValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string title = (string)value ?? "";
            if (title.Trim().Length == 0)
            {
                return new ValidationResult(false, App.Current.Resources["MailValidationEmpty"].ToString());
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
    public class BodyValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string body = (string)value ?? "";
            if (body.Trim().Length == 0)
            {
                return new ValidationResult(false, App.Current.Resources["MailValidationEmpty"].ToString());
            }
            else if (body.Length < 10)
            {
                return new ValidationResult(false, App.Current.Resources["MailValidationShort"].ToString());
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
