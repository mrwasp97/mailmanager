﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using wpfTask1;

namespace MailManager.ViewModels
{
    public class MainViewModel : ViewModel
    {
        private const char SEPARATOR = ' ';

        private bool isLogged;
        public bool IsLogged
        {
            get
            {
                return isLogged;
            }
            set
            {
                isLogged = value;
                OnPropertyChanged("IsLogged");
            }
        }

        private string searchPhrase;
        public string SearchPhrase
        {
            get
            {
                return searchPhrase;
            }
            set
            {
                searchPhrase = value;
                OnPropertyChanged("SearchPhrase");
            }
        }

        private EmailUser loggedUser;
        public EmailUser LoggedUser
        {
            get
            {
                return loggedUser;
            }
            set
            {
                loggedUser = value;
                OnPropertyChanged("LoggedUser");
            }
        }

        private EmailMessage selectedMessage;
        public EmailMessage SelectedMessage
        {
            get
            {
                return selectedMessage;
            }
            set
            {
                selectedMessage = value;
                OnPropertyChanged("SelectedMessage");
            }
        }

        private MessageCollection receivedMessages;
        private MessageCollection sentMessages;

        public void Login(EmailUser user)
        {
            IsLogged = true;
            LoggedUser = user;
            receivedMessages = new MessageCollection(LoggedUser.MessagesReceived);
            sentMessages = new MessageCollection(LoggedUser.MessagesSent);
        }

        public void Logout()
        {
            IsLogged = false;
            LoggedUser = null;
            receivedMessages = null;
            sentMessages = null;
            SelectedMessage = null;
            SearchPhrase = "";
        }

        private string[] SearchPhrases()
        {
            if (searchPhrase == null)
            {
                return new string[] { "" };
            }
            else
            {
                return searchPhrase.Split(new char[] { SEPARATOR });
            }
        }

        public MessageCollection FilterReceivedMessages()
        {
            bool matcher(EmailMessage message, string phrase)
            {
                return message.Title.Contains(phrase)
                        || message.Date.Contains(phrase)
                        || message.From.Contains(phrase);
            }

            return FilterMessages(receivedMessages, matcher);
        }

        public MessageCollection FilterSentMessages()
        {
            bool matcher(EmailMessage message, string phrase)
            {
                return message.Title.Contains(phrase)
                        || message.Date.Contains(phrase)
                        || message.To.Contains(phrase);
            }

            return FilterMessages(sentMessages, matcher);
        }

        public void SendMessage(EmailMessage emailMessage)
        {
            sentMessages.Add(emailMessage);
        }

        public void DeleteReceived(EmailMessage emailMessage)
        {
            receivedMessages.Remove(emailMessage);
        }

        public void DeleteSent(EmailMessage emailMessage)
        {
            sentMessages.Remove(emailMessage);
        }

        private MessageCollection FilterMessages(MessageCollection messages, MatchPhrase matcher)
        {
            var filteredMessages = new MessageCollection();
            messages = messages ?? new MessageCollection();

            if (searchPhrase == null || searchPhrase == "")
            {
                foreach (EmailMessage message in messages)
                {
                    filteredMessages.Add(message);
                }
                return filteredMessages;
            }

            string[] phrases = SearchPhrases();
            
            foreach (EmailMessage message in messages)
            {
                foreach (string phrase in phrases)
                {
                    if (phrase != "" && matcher(message, phrase))
                    {
                        filteredMessages.Add(message);
                    }
                }
            }

            return filteredMessages;
        }



        private MessageCollection FilterMessages(ObservableCollection<EmailMessage> messages)
        {
            var filteredMessages = new MessageCollection();

            if (searchPhrase == null)
            {
                foreach (EmailMessage message in messages)
                {
                    filteredMessages.Add(message);
                }
                return filteredMessages;
            }
            string[] phrases = searchPhrase.Split(new char[] { SEPARATOR });


            foreach (EmailMessage message in messages)
            {
                foreach (string phrase in phrases)
                {
                    if (message.Title.Contains(phrase)
                        || message.Date.Contains(phrase)
                        || message.From.Contains(phrase))
                    {
                        filteredMessages.Add(message);
                    }
                }
            }

            return filteredMessages;
        }

        private delegate bool MatchPhrase(EmailMessage message, string phrase);


    }

    public class FromConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string from = (string)value ?? "";
            return from.Length == 0 ? "" : $"From: {from}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ToConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string to = (string)value ?? "";
            return to.Length == 0 ? "" : $"To: {to}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MessageCollection : ObservableCollection<EmailMessage>
    {
        public MessageCollection(ObservableCollection<EmailMessage> messages = null) : base()
        {
            if (messages != null)
            {
                foreach (EmailMessage message in messages)
                {
                    Add(message);
                }
            }
        }

        public MessageCollection() : base()
        {
        }
    }
}
